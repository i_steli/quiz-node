const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const db = require('./config/db');
const errorHandler = require('./src/Errors/ErrorHandler');
const cookieParser = require('cookie-parser');
const xss = require('xss-clean');
const mongoose = require('mongoose');
const logger = require('./config/logger');
const courseRoutes = require('./config/routes/courses');
const quizRoutes = require('./config/routes/quizzes');
const userRoutes = require('./config/routes/users');
const questionRoutes = require('./config/routes/questions');
const otherRoutes = require('./config/routes/others');
const socket = require('./config/sockets/socket');

require('dotenv').config({
    path: '.env',
    debug: JSON.parse(process.env.DEBUG)
});

db.mongoDB();

mongoose.set('debug', (collectionName, methodName, query, doc) => {
    // remove fields
    const cloneQuery = {...query};
    delete cloneQuery.password;
    delete doc.session;

    const queryString = JSON.stringify(cloneQuery);
    const queryDoc    = JSON.stringify(doc);
    const message     = `${collectionName}.${methodName} | ${queryString} | ${queryDoc}`

    logger.info(message);
});

app.use(cookieParser());
app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(xss());

// set CORS headers
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    next();
})

// routes
app.use('/api/v1/users', userRoutes);
app.use('/api/v1/courses', courseRoutes)
app.use('/api/v1/quizzes', quizRoutes)
app.use('/api/v1/questions', questionRoutes);
app.use('/api/v1', otherRoutes);

// error handler
app.use(errorHandler)

const server = app.listen(process.env.SERVER_PORT,function () {
    console.log("docker logs -f <container_id>");
    console.log(`Server started: localhost with port ${process.env.SERVER_PORT}`)
})

socket.init(server);

socket.getIO().on("connection", (socket) => {
    console.log('Socket Client connected')
});
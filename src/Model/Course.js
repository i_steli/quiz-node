const mongoose = require('mongoose');
const slugify = require('slugify');

const CourseSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'Name is required'],
    },
    slug: {
        type: String,
        unique: true,
    },
    type: {
        type: String,
        trim: true,
        required: [true, 'Type is required'],
    },
    description: {
        type: String,
        trim: true,
        required: false
    },
    active: {
        type: Boolean,
        default: true
    },
}, {
    toJSON: {
        virtuals: true,
        transform: function (doc, ret) {
            delete ret._id;
        }
    }, // So `res.json()` and other `JSON.stringify()` functions include virtuals
    toObject: { virtuals: true }, // So `console.log()` and other functions that use `toObject()` include virtuals
    timestamps: true
})

CourseSchema.pre('save', async function (next) {
    this.slug = slugify(this.name, {
        lower: true,
        trim: true
    });
    next();
});

CourseSchema.virtual('quizzes', {
    ref: 'Quiz',
    localField: '_id',
    foreignField: 'course'
})

module.exports = mongoose.model('Course', CourseSchema);

const mongoose = require('mongoose');

const CountrySchema = new mongoose.Schema({
    countryId: {
        type: Number,
        required: [true, 'Number is required'],
        unique: true,
    },
    name: {
        type: String,
        required: [true, 'Name is required'],
        trim: true,
    },
    locale: {
        type: String,
        validate: {
            validator: function (v) {
                return /[^\W\d]{2}/.test(v)
            },
            message: props => `${props.value} is not a valid locale`,
        },
        required: [true, 'Locale is required'],
        trim: true,
    },
}, {
    toJSON: {
        transform: function(doc, ret) {
            // ret.id = ret._id;
            delete ret._id;
        }
    },
    timestamps: true
})

module.exports = mongoose.model('Country', CountrySchema);
const mongoose = require('mongoose');
const slugify = require('slugify');
const Course = require('./Course');
const moment = require('moment');

const QuizSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required'],
        trim: true,
    },
    slug: {
        type: String,
        unique: true,
    },
    course: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Course',
        validate:
            [
                {
                    validator: async function(value) {
                        const course = await Course.findById(value);

                        if (! course) return false;
                    },
                    msg: 'Please choose a valid course'
                }
            ],
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    active: {
        type: Boolean,
        default: true
    },
    timeLimit: {
        type: Number,
        default: 1000 * 60 * 30, // ms => 30m
    },
    noQuestions: {
        type: Number,
        required: [true, 'No of questions is required']
    },
}, {
    toJSON: {
        transform: function(doc, ret) {
            // ret.id = ret._id;
            delete ret._id;
            if (ret.createdAt) {
                ret.createdAt = moment(ret.createdAt, "YYYYMMDD").fromNow();
            }
        },
        virtuals: true
    }, // So `res.json()` and other `JSON.stringify()` functions include virtuals
    toObject: {
        transform: function(doc, ret) {
            // ret.id = ret._id;
            delete ret._id;
            if (ret.createdAt) {
                ret.createdAt = moment(ret.createdAt, "YYYYMMDD").fromNow();
            }
        },
        virtuals: true
    }, // So `console.log()` and other functions that use `toObject()` include virtuals
    timestamps: true
})

QuizSchema.pre('save', async function (next) {
    this.slug = slugify(this.name, {
        lower: true,
        trim: true
    });
    next();
});

QuizSchema.virtual('questions', {
    ref: 'Question',
    localField: '_id',
    foreignField: 'quiz'
});

QuizSchema.virtual('user', {
    ref: 'User',
    localField: 'createdBy',
    foreignField: '_id',
    justOne: true,
})

module.exports = mongoose.model('Quiz', QuizSchema);

const mongoose = require('mongoose');
const Status = require('../Enum/SessionStatus');
const SessionStatus = require('../Enum/SessionStatus');
const Helper = require('../Utils/Helper');
const Question = require('../Model/Question');
const QuestionFactory = require('../Factory/QuestionTypeFactory');

const QuizSessionSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        index: true,
    },
    status: {
        type: String,
        enum: Status
    },
    score: {
        type: Number,
        default: 0,
        max: 100,
    },
    layout: {
        quiz: {
            name: String,
            course: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Course'
            },
            timeLimit: Number,
            noQuestions: Number,
        },
        questions: []
    },
    timeStart: {
        type: Date,
        default: Date.now,
    },
    timeFinish: {
        type: Date,
        default: null,
    }
});

QuizSessionSchema.static('finish', async function(session, questionAnswers, noQuestions) {
    let questions       = [];
    let layoutQuestions = [];
    let correctAnswers  = 0;
    let score           = 0;

    const questionIds = Object.keys(questionAnswers);

    if (questionIds.length > 0) {
        questions = await Question
            .find({
                _id: {
                    $in: questionIds
                },
            })
            .select([
                '_id',
                'title',
                'type',
                'variants'
            ]);


        layoutQuestions = questions.map(question => {
            const answer         = questionAnswers[question._id];
            const questionType   = new QuestionFactory(question);

            const isCorrect = questionType.isCorrect(answer);

            if (isCorrect) {
                correctAnswers++;
            }

            return {
                title: question.title,
                type: question.type,
                variants: question.variants,
                answer,
                isCorrect,
            }
        })
    }

    score = Helper.precision((correctAnswers * 100) / noQuestions, 2);

    await this.updateOne({_id: session}, {
        score: score,
        status: SessionStatus.COMPLETED,
        timeFinish: Date.now(),
        "layout.questions": layoutQuestions
    })
})

module.exports = mongoose.model('QuizSession', QuizSessionSchema);
const mongoose = require('mongoose');
const VariantTitleValidation = require('../Errors/VariantTitleValidation');

const VariantSchema = new mongoose.Schema({
    title: {
        type: String,
        validate: {
            validator: function(value) {
                if ( !this.text && !value ) {
                    throw new VariantTitleValidation('Variant Title is required');
                }
            },
        },
        trim: true,
    },
    text: {
        type: String,
        default: null,
    },
    is_correct: {
        type: Boolean,
        required: [true, 'Check answer if it is correct']
    },
});

module.exports = {
    VariantSchema: VariantSchema,
    Variant: mongoose.model('Variant', VariantSchema),
};
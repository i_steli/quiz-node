const mongoose = require('mongoose');
const { VariantSchema }  = require('./Variant');
const { TYPES } = require('../Enum/Question');
const moment = require('moment');

const QuestionSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Title is required'],
        trim: true
    },
    type: {
        type: String,
        enum: Object.values(TYPES),
        required: [true, 'Type is required']
    },
    active: {
        type: Boolean,
        default: false,
    },
    variants: {
        type:  [VariantSchema],
        validate: {
            validator: function(v) {
                return v.length !== 0;
            },
            message: props => `Please add a variant`
        },
    },
    course: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Course',
        required: [true, 'Course is required'],
    },
    quiz: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Quiz',
        default: null,
    }
}, {
    timestamps: true,
    toJSON: {
        transform: function(doc, ret) {
            ret.id = doc._id;
            delete ret._id;
            if (ret.createdAt) {
                ret.createdAt = moment(ret.createdAt, "YYYYMMDD").fromNow()
            }
        }
    }
});

module.exports = mongoose.model('Question', QuestionSchema);
const mongoose = require('mongoose');
const bcrypt   = require('bcrypt');
const jwt      = require('jsonwebtoken');
const fs       = require('fs');
const path     = require('path');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required'],
        trim: true,
    },
    role: {
        type: String,
        required: [true, 'Role is required'],
        enum: ['developer', 'admin', 'user'],
        default: 'user',
        lowercase: true,
    },
    country: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Country',
        required: [true, 'Country is required'],
    },
    email: {
        type: String,
        required: [true, 'Email is required'],
        unique: true,
        trim: true,
        match: [
            /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
            'Please add a valid email'
        ]
    },
    gender: {
        type: String,
        enum: ['M', 'F', 'N/A'],
        required: [true, 'Please add a gender']
    },
    password: {
        type: String,
        required: [true, 'Please add a password'],
        minlength: 6,
        select: false
    },
}, {
    timestamps: true
})

UserSchema.pre('save', async function(next) {
    if (! this.isModified('password')) {
        next();
    }

    const salt = await bcrypt.genSalt(parseInt(process.env.SALT));
    this.password = await bcrypt.hash(this.password, salt);
})

UserSchema.methods.getToken = function(options = {}) {
    const secretKey = fs.readFileSync(path.join(__dirname, '../../jwtRS256.key'), {
        encoding: 'utf-8',
        flag: 'r'
    });

    if (Object.keys(options).length === 0) {
        options = {
            algorithm: process.env.TOKEN_AlG,
            expiresIn: process.env.TOKEN_EXPIRES
        }
    }

    return jwt.sign({
        id: this.id,
        email: this.email
    }, secretKey, options)
}

UserSchema.methods.passwordIsMatched = async function(password) {
    return await bcrypt.compare(password.toString(), this.password);
}

module.exports = mongoose.model('User', UserSchema);
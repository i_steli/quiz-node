const asyncHandler = require('../Utils/AsyncHandler');
const Country = require('../Model/Country');

/**
 * @description Home
 * @route GET /api/v1
 * @access Public
 */
exports.home = asyncHandler(async (req, res, next) => {
    res.status(200).json({
        success: true,
        message: req.user
    })
})

exports.createCountry = asyncHandler(async (req, res, next) => {
    const country = await Country.create(req.body)

    res.status(200).json({
        success: true,
        item: country
    })
})

exports.getCountries = asyncHandler(async (req, res, next) => {
    const countries = await Country.find({}).select('countryId name');

    res.status(200).json({
        success: true,
        items: countries
    })
})
const User = require('../Model/User');
const Country = require('../Model/Country');
const asyncHandler = require('../Utils/AsyncHandler');
const ValidationError = require('../Errors/ValidationError');
const UnauthorizedError = require('../Errors/UnauthorizedError');
const Helper = require('../Utils/Helper');

/**
 * @description Register User
 * @route POST /api/v1/users/register
 * @access Public
 */
exports.register = asyncHandler(async (req, res, next) => {
    const country = await Country.findOne({countryId: req.body.country})

    req.body.country = country?.id || null;

    const user = await User.create(req.body);

    const token = user.getToken();

    return res.status(201)
        .json({
            success: true,
            token,
            expire: Helper.convertTimestamp(process.env.TOKEN_EXPIRES),
        })
});

/**
 * @description Login User
 * @route POST /api/v1/login
 * @access Public
 */
exports.login = asyncHandler(async (req, res, next) => {
    const {email, password} = req.body;

    if (! email || ! password) {
        return next(new ValidationError([
            {
                path: 'email_password',
                message: 'Email or password are required',
            }
        ]));
    }

    const user = await User.findOne({email: req.body.email}).select([
        '+password'
    ]);

    if (! user) {
        return next(new UnauthorizedError('User not found'))
    }

    const matched = await user.passwordIsMatched(password);

    if (! matched) {
        return next(new UnauthorizedError('User not found'))
    }

    const token = user.getToken();

    return res.status(200)
        .json({
            success: true,
            token,
            expire: Helper.convertTimestamp(process.env.TOKEN_EXPIRES),
        })
})

/**
 * @description Get User
 * @route GET /api/v1/user
 * @access Private
 */
exports.getUser = asyncHandler(async (req, res, next) => {
    return res.status(201)
        .json({
            success: true,
            user: req.user
        })
});
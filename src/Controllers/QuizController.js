const asyncHandler = require('../Utils/AsyncHandler');
const Quiz = require('../Model/Quiz');
const Question = require('../Model/Question');
const QuizSession = require('../Model/QuizSession');
const NotFoundError = require('../Errors/NotFoundError');
const UnauthorizedError = require('../Errors/UnauthorizedError');
const GeneralError = require('../Errors/GeneralError');
const Pagination = require('../Service/Pagination');
const Roles = require('../Enum/Roles');
const SessionStatus = require('../Enum/SessionStatus');
const mongoose = require('mongoose');
const io = require('../../config/sockets/socket')

/**
 * @description Create Course
 * @route POST /api/v1/courses
 * @access Private
 */
exports.createQuiz = asyncHandler(async (req, res, next) => {
    req.body.createdBy = req.user

    if (req.body.course) {
        req.body.course = req.body.course.trim();
    }

    const quiz = await Quiz.create(req.body);

    res.status(201).json({
        success: true,
        data: {
            item: quiz
        }
    });
});

/**
 * @description Get Quiz
 * @route GET /api/v1/quizzes/:slug/start
 * @access Protected
 */
exports.startQuiz = asyncHandler(async (req, res, next) => {
    const quiz = await Quiz
        .aggregate([
            {
                $project: {
                    _id: 1,
                    slug: 1,
                    questions: 1,
                    course: 1,
                    timeLimit: 1,
                    noQuestions: 1,
                    name: 1,
                }
            },
            {
                $match: {slug: req.params.slug}
            },
            {
                $limit: 1
            },
            {
                $lookup: {
                    from: 'questions',
                    localField: '_id',
                    foreignField: 'quiz',
                    as: 'questions'
                }
            },
            {
                $addFields: {
                    "questions_count": {
                        $size: "$questions"
                    }
                }
            },
        ])

    if (! quiz.length) {
        return next(new NotFoundError('Quiz not found'))
    }

    const firstQuiz = quiz[0];

    if (firstQuiz.noQuestions !== firstQuiz.questions_count) {
        return next(new GeneralError('The quiz is incomplete. Please contact an administrator'))
    }

    const quizSession = new QuizSession({
        user: req.user,
        status: SessionStatus.IN_PROGRESS,
        layout: {
            quiz: {
                name: firstQuiz.name,
                course: firstQuiz.course,
                timeLimit: firstQuiz.timeLimit,
                noQuestions: firstQuiz.noQuestions,
            }
        }
    });

    await quizSession.save();

    // start counter event
    const startSessionCounter = setTimeout( function() {
        io.getIO().emit(`'close-quiz-session-'${quizSession._id}`, {session: quizSession._id});
        clearTimeout(startSessionCounter);
    },   firstQuiz.timeLimit)

    res.status(200).json({
        success: true,
        data: {
            item: {
                session: quizSession._id,
                ...firstQuiz
            }
        }
    });
});

exports.finishQuiz = asyncHandler(async (req, res, next) => {
   const quizSession     = await QuizSession.findById(req.body.session);
   const questionAnswers = req.body?.answers || {}

   if (! quizSession) {
       throw new NotFoundError('Quiz not found');
   }

   if (
       quizSession?.user !== req.user
       && req.user.role !== Roles.ADMIN
   ) {
       throw new UnauthorizedError('You are not allowed to complete this quiz')
   }

   QuizSession.finish(req.body.session, questionAnswers, quizSession.layout.quiz.noQuestions)

    res.status(200).json({
        success: true,
        data: {
            message: 'Session has been closed'
        }
    });
});

/**
 * @description Get Quiz
 * @route GET /api/v1/quizzes/:slug/show
 * @access Protected
 */
exports.getQuiz = asyncHandler(async (req, res, next) => {
    const quiz = await Quiz.findOne({slug: req.params.slug})
        .populate([
            {
                path: 'user',
                select: ['name', 'id']
            },
            {
                path: 'questions',
                select: ['id', '-quiz']
            }
        ]);

    res.status(200).json({
        success: true,
        data: {
            item: quiz
        }
    });
})


/**
 * @description Get Quizzes | Get Course Quizzes
 * @route GET /api/v1/courses/:courseId/quizzes | api/v1/quizzes
 * @access Protected
 */
exports.getQuizzes = asyncHandler(async (req, res, next) => {
    let quizzes;

    if (req.params.courseId) {
        quizzes = Quiz.find({course: req.params.courseId})
            .select([
            'name',
        ])
            .populate({
                path: 'questions',
                match: {
                    active: true
                }
            });
    } else {
        quizzes = Quiz.find({})
            .select([
            'name',
            'course',
            'active',
            'slug',
            'noQuestions',
            'createdAt'
        ])
            .populate({
                path: 'course',
                select: 'name',
            })
            .sort('-createdAt')
    }

    if (req.query.query) {
        quizzes.find({
            name: {
                $regex: req.query.query,
                $options: 'i',
            }
        })
    }

    const pagination = new Pagination(quizzes, req.query.page, req.query.perPage);
    const results    = await pagination.get();

    res.status(200).json({
        success: true,
        data: {
            ...results,
        }
    })
})

/**
 * @description Get Quizzes | Get Course Quizzes
 * @route GET /api/v1/quizzes/:id/update
 * @access Protected
 */
exports.updateQuiz = asyncHandler(async (req, res, next) => {
    const session = await mongoose.startSession();
    const quiz    = await Quiz.findById(req.params.id).populate({
        path: 'questions',
        select: ['id', '-quiz']
    }).session(session);

    if (! quiz) {
        throw new NotFoundError('Quiz not found');
    }

    if (req.body.noQuestions < req.body.questions.length) {
        throw new GeneralError('No of questions must be higher than attached questions');
    }

    quiz.name        = req.body.name;
    quiz.course      = req.body.course || null;
    quiz.active      = req.body.active;
    quiz.timeLimit   = req.body.timeLimit;
    quiz.noQuestions = req.body.noQuestions;

    session.startTransaction();
    try {
        await quiz.save();

        if (req.body?.questions) {
            const questions         = quiz?.questions || []
            const detachedQuestions = questions.map(question => question.id);
            const attachedQuestions = req.body?.questions || [];

            await Question.updateMany({
                _id: { $in: detachedQuestions }
            }, {quiz: null}).session(session)

            await Question.updateMany({
                _id: { $in: attachedQuestions }
            }, {quiz: quiz.id}).session(session)

        }

        await session.commitTransaction();
    } catch (error) {
        console.error('abort transaction', error);
        await session.abortTransaction();
        await session.endSession();
        throw error;
    }

    await session.endSession();

    res.status(200).json({
        success: true,
        data: {
            item: quiz,
        }
    })
});

/**
 * @description Delete Quizzes
 * @route DELETE /api/v1/quizzes
 * @access Protected
 */
exports.deleteQuiz = asyncHandler(async (req, res, next) => {
    const session = await mongoose.startSession();
    session.startTransaction();

    if (req.body.items.length === 0) {
        return;
    }

    let filter = {
        _id: { $in: req.body.items }
    };

    if (req.user.role !== Roles.ADMIN) {
        filter.createdBy = req.user.id;
    }

    let message;

    try {
        let availableQuizzes = await Quiz.find(filter).select('_id');

        availableQuizzes = availableQuizzes.map(quiz => quiz.id);

        const itemsDeleted = await Quiz.deleteMany(filter).session(session);

        if (itemsDeleted === 0 || itemsDeleted?.deletedCount) {
            message = 'Quiz not found';
        } else if (itemsDeleted?.deletedCount > 1) {
            message = 'Quizzes have been deleted';
        } else {
            message = 'Quiz has been deleted';
        }

        await Question.updateMany({
            quiz: { $in: availableQuizzes}
        }, { quiz: null })
            .session(session)

        await session.commitTransaction();
    } catch (error) {
        console.error('abort transaction', error);
        message = error.message;
        await session.abortTransaction();
    }

    await session.endSession();

    res.status(200).json({
        success: true,
        data: {
            message
        }
    })
});
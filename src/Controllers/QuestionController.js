const asyncHandler = require('../Utils/AsyncHandler');
const Question = require('../Model/Question');
const Quiz = require('../Model/Quiz');
const GeneralError = require('../Errors/GeneralError');
const EnumQuestion = require('../Enum/Question');
const Pagination = require('../Service/Pagination');

/**
 * @description Create Question
 * @route POST /api/v1/questions
 * @access Private
 */
exports.createQuestions = asyncHandler(async (req, res, next) => {
    let noQuestions = 0;
    let quiz = null;

    if (req.body.quiz) {
        noQuestions = await Question.countDocuments({ quiz: req.body.quiz });
        quiz  = await Quiz.findOne({id: req.body.quiz}).select('noQuestions');
    }

    if (quiz && noQuestions > quiz.noQuestions) {
        return next(new GeneralError('Please attach another quiz, this one has reached its limit.'));
    }

    const question = await Question.create(req.body);

    res.status(201).json({
        success: true,
        data: {
            item: question
        }
    });
});

/**
 * @description Get Questions
 * @route POST /api/v1/questions
 * @access Private
 */
exports.getQuestions = asyncHandler(async (req, res, next) => {
    console.log('pagination query', req.query);
    const questions = Question.find({}).populate({
        path: 'quiz',
        select: 'name',
    })
        .select([
            '-course',
            '-variants',
            '-updatedAt'
        ]);

    if(req.query?.query) {
        questions.find({title: {
                $regex: req.query.query,
                $options: 'i',
            }
        })
    }

    const pagination = new Pagination(questions, req.query.page, req.query.perPage);
    const results    = await pagination.get();

    res.status(200).json({
        success: true,
        data: {
            ...results
        }
    })
})

/**
 * @description Get Question Types
 * @route POST /api/v1/questions/types
 * @access Private
 */
exports.getTypes = asyncHandler(async (req, res, next) => {
    res.status(200).json({
        success: true,
        data: {
            items: Object.values(EnumQuestion.TYPES)
        }
    })
})



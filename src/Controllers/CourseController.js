const asyncHandler = require('../Utils/AsyncHandler');
const Course = require('../Model/Course');

/**
 * @description Create Course
 * @route POST /api/v1/courses
 * @access Private
 */
exports.createCourse = asyncHandler(async (req, res, next) => {
    const course = await Course.create(req.body);

    res.status(201).json({
        success: true,
        data: {
            item: course
        }
    });
});

/**
 * @description Get Courses
 * @route GET /api/v1/courses/slug
 * @access Public
 */
exports.getCourse = asyncHandler(async (req, res, next) => {
    const course = await Course
        .findOne({slug: req.params.slug})
        .populate({
            path: 'quizzes',
            select: [
                'created_by',
                'time_limit'
            ]
        })

    res.status(200).json({
        success: true,
        data: {
            item: course
        }
    });
});

/**
 * @description Get Courses
 * @route GET /api/v1/courses
 * @access Public
 */

exports.getCourses = asyncHandler(async (req, res, next) => {
    const courses = await Course.find({}).select([
        'name',
    ]);

    res.status(200).json({
        success: true,
        data: {
            items: courses
        }
    });
})


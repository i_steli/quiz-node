const jwt = require('jsonwebtoken');
const fs  = require('fs');
const path  = require('path');
const UnauthorizedError = require('../Errors/UnauthorizedError');
const User = require('../Model/User');

exports.authentication = (populate = []) => async (req, res, next) => {
    const headerToken = req.headers.authorization?.substr(7);
    const token       = headerToken || req.cookies.token || false;

    if (! token) {
        return next(new UnauthorizedError('Token is invalid'));
    }

    const secretKey = fs.readFileSync(path.join(__dirname, '../../jwtRS256.key'), {
        encoding: 'utf-8',
        flag: 'r'
    });

    try {
        const decoded = jwt.verify(token, secretKey,{ algorithms: [process.env.TOKEN_AlG] })

        let user;
        if (populate.length === 0) {
            user = await User.findById(decoded.id);
        } else {
            user = await User.findById(decoded.id).populate(populate);
        }

        if (! user) {
            return next(new UnauthorizedError('Token is invalid'))
        }

        req.user = user;
    } catch (error) {
        return next(new UnauthorizedError('Token is invalid'))
    }

    next();
}

// middleware is called like roles(roles)(req, res, next)
exports.roles = (roles, redirect = null) => (req, res, next) => {
    if (! roles.includes(req.user.role)) {
        return next(new UnauthorizedError('Role is not valid', redirect))
    }

    next();
};
class Helper
{
    static convertTimestamp(value)
    {
        let parsedValue = value;
        let d = new Date();

        if (value.endsWith('d')) {
            parsedValue = value.match(/\d+/);

            if (! parsedValue) {
                throw new Error('Expiration date is not a valid integer')
            }

            parsedValue = parseInt(parsedValue[0]);
        }

        return d.setDate(d.getDate() + parsedValue)
    }

    static precision(value, decimals) {
        const noDecimals = 10**decimals;

        return Math.round(value * noDecimals) / noDecimals;
    }

}

module.exports = Helper;
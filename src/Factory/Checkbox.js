const { TYPES } = require('../Enum/Question');

class Checkbox
{
    static type = TYPES.CHECKBOX;

    constructor(question) {
        this.question = question;
    }

    isCorrect(answers) {
        const correctVariants = [];

        for (const variant of this.question.variants) {
            if (variant.is_correct) {
                correctVariants.push(variant['title'])
            }
        }

        for (const answer of answers) {
            if (! correctVariants.includes(answer)) {
                return false;
            }
        }

        return true;
    }

}

module.exports = Checkbox;
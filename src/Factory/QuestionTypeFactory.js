const { TYPES } = require('../Enum/Question');
const Radio = require('./Radio');
const Text = require('./Text');
const Checkbox = require('./Checkbox');

class QuestionTypeFactory
{
    constructor(question) {
        this.type = question.type;

        let objType;

        switch (question.type) {
            case TYPES.RADIO:
                objType = new Radio(question);
                break;
            case TYPES.CHECKBOX:
                objType = new Checkbox(question);
                break;
            case TYPES.TEXT:
                objType = new Text(question);
                break
        }

        return objType;
    }

}

module.exports = QuestionTypeFactory;
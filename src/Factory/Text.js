const { TYPES } = require('../Enum/Question');

class Text
{
    static type = TYPES.TEXT;

    constructor(question) {
        this.question = question;
    }

    isCorrect(answer) {
        const variantField   = 'text';
        const correctVariant = this.question.variants.find(variant => variant.is_correct);
        const variantAnswer  = correctVariant[variantField];

        return variantAnswer.toString() === answer.toString();
    }

}

module.exports = Text;
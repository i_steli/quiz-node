const { TYPES } = require('../Enum/Question');

class Radio
{
    static type = TYPES.RADIO;

    constructor(question) {
        this.question = question;
    }

    isCorrect(answer) {
        const variantField   = 'title';
        const correctVariant = this.question.variants.find(variant => variant.is_correct);
        const variantAnswer  = correctVariant[variantField];

        return variantAnswer.toString() === answer.toString();
    }

}

module.exports = Radio;
'use strict';

class Pagination
{
    static PAGE_DEFAULT = 1;
    static PER_PAGE_DEFAULT = 10;

    #page;
    #perPage;
    #query;

    constructor(query, page, perPage)
    {
        this.#query   = query;
        this.#page    = parseInt(page) || Pagination.PAGE_DEFAULT;
        this.#perPage = parseInt(perPage) || Pagination.PER_PAGE_DEFAULT;
    }

    async getTotal()
    {
        return await this.#query.clone().countDocuments();
    }

    async get()
    {
        const totalDocs = await this.getTotal()
        const totalPages = Math.ceil(totalDocs / this.#perPage);

        const results = await this.#query
            .skip((this.#page - 1) * this.#perPage)
            .limit(this.#perPage);

        return {
            pagination: {
                page: this.#page,
                total: totalDocs,
                totalPages: totalPages,
                perPage: this.#perPage,
            },
            items: results
        }
    }

}

module.exports = Pagination;
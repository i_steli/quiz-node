class ValidationError extends Error
{
    static code = 400;

    constructor(errors = {}, message = '') {
        super(message);
        this.name   = 'ValidationError';
        this.errors = errors;
    }

}

module.exports = ValidationError;
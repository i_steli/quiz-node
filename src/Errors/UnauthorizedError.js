class UnauthorizedError extends Error
{
    static code = 401;

    constructor(message = '', redirectUrl = null) {
        super(message);
        this.name        = 'UnauthorizedError';
        this.redirectUrl = redirectUrl;
    }

}

module.exports = UnauthorizedError;
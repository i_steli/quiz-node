class ApiErrorResponse extends Error
{
    constructor(message, statusCode, errors = [], redirectUrl = null) {
        super(message);
        this.statusCode  = statusCode;
        this.redirectUrl = redirectUrl;
        // static methods are defined as props of the constructor instead as props of the prototype object
        this.type = this.constructor.getType(statusCode);
        this.parseErrors(errors)
    }

    static getType(statusCode) {
        let type;

        switch (statusCode) {
            case 401:
                type = 'PERMISSION DENIED';
                break;
            case 404:
            case 400:
                type = 'VALIDATION'
                break;
            default:
                type = 'GENERAL ERROR'
        }

        return type;
    }

    parseErrors(errors) {
        this.errors = [];

        if (Object.keys(errors).length === 0) return;

        Object.values(errors).map(err => {
            let parsedError = {};
            parsedError.field = err.reason?.path || err.path;
            parsedError.message = err.message;
            parsedError.params = err.properties?.enumValues || []

            this.errors.push(parsedError);
        })

    }

     response(res) {
        return res.status(this.statusCode).json({
            success: false,
            type: this.type,
            message: this.message,
            errors: this.errors,
            redirectUrl: this.redirectUrl
        });
    }

}

module.exports = ApiErrorResponse;
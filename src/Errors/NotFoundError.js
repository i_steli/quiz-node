class NotFoundError extends Error
{
    static code = 404;

    constructor(message = '') {
        super(message);
        this.name   = 'NotFoundError';
    }

}

module.exports = NotFoundError;
const ApiErrorResponse = require('./ApiErrorResponse');
const ValidationError = require('./ValidationError');
const GeneralError = require('./GeneralError');
const NotFoundError = require('./NotFoundError');
const UnauthorizedError = require('./UnauthorizedError');
const path = require('path');
const logger = require(path.join(process.cwd(), 'config', 'logger'));

const errorHandler = function (err, req, res, next) {
    if (process.env.DEBUG === 'true') {
        console.log(err);
    }

    let error;
    let type = err.name;

    if (type === 'MongoServerError') {
        type = err.code;
    }

    console.log('error type', type);

    switch (type) {
        case 'ValidationError':
            error = new ApiErrorResponse('', ValidationError.code, err.errors);
            break;
        case 'CastError':
            error = new ApiErrorResponse(err.message, GeneralError.code);
            break;
        case 'UnauthorizedError':
            error = new ApiErrorResponse(err.message, UnauthorizedError.code, [], err.redirectUrl);
            break;
        case 'NotFoundError':
            error = new ApiErrorResponse(err.message, NotFoundError.code);
            break;
        case 11000:
            error = new ApiErrorResponse('Duplicate key', GeneralError.code)
            break;
        default:
            const message = type === 'GeneralError' ? err.message : 'Something went wrong! Please contact the administrator';
            error = new ApiErrorResponse(message, GeneralError.code);
    }

    logger.error(err.stack);

    error.response(res);
}

module.exports = errorHandler;
class GeneralError extends Error
{
    static code = 500;

    constructor(message = '') {
        super(message);
        this.name = 'GeneralError';
    }

}

module.exports = GeneralError;
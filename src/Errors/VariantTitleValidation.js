const mongoose = require('mongoose');

class VariantTitleValidation extends mongoose.Error.ValidatorError
{
    constructor(message = '') {
        const properties = {
            message: message,
            path: 'variants',
        }
        super(properties);
    }
}

module.exports = VariantTitleValidation;
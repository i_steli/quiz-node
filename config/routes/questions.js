const express = require('express');
const router = express.Router();

const { authentication, roles } = require('../../src/Middleware/Permissions')

const {
    createQuestions,
    getTypes,
    getQuestions,
} = require('../../src/Controllers/QuestionController')

router
    .route('/')
    .get(authentication(), getQuestions)
    .post(authentication(), roles(['admin']), createQuestions);

router.get('/types', authentication(), getTypes);

module.exports = router;

const express = require('express');
const router = express.Router({mergeParams: true});

const { authentication, roles } = require('../../src/Middleware/Permissions')

const {
    createQuiz,
    getQuiz,
    startQuiz,
    getQuizzes,
    updateQuiz,
    deleteQuiz,
    finishQuiz,
} = require('../../src/Controllers/QuizController')

router
    .route('/')
    .get(authentication(), getQuizzes)
    .delete(authentication(), deleteQuiz)
    .post(authentication(), roles(['admin', 'developer']), createQuiz);

router.get('/:slug/show', authentication(), getQuiz);
router.get('/:slug/start', authentication(), startQuiz);
router.put('/:id/update', authentication(), updateQuiz)
router.patch('/finish', authentication(), finishQuiz)

module.exports = router;
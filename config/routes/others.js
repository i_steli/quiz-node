const express = require('express');
const router = express.Router();

const { authentication, roles } = require('../../src/Middleware/Permissions')
const {
    home,
    createCountry,
    getCountries,
} = require('../../src/Controllers/HomeController')

router.get('/', authentication, roles(['user']), home);

router.route('/country')
    .post( authentication, roles(['admin']), createCountry)
    .get(getCountries);

module.exports = router;
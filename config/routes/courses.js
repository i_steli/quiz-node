const express = require('express');
const router = express.Router();

const { authentication, roles } = require('../../src/Middleware/Permissions')

const {
    createCourse,
    getCourse,
    getCourses,
} = require('../../src/Controllers/CourseController')

// Include other resource routes
const quizRouter = require('./quizzes');

router.use('/:courseId/quizzes', quizRouter);

router
    .route('/')
    .post(authentication(), roles(['admin']), createCourse)
    .get(getCourses);
router.get('/:slug', getCourse);

module.exports = router;

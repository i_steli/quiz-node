const express = require('express');
const router = express.Router();

const { authentication, roles } = require('../../src/Middleware/Permissions')

const {
    register,
    login,
    getUser,
} = require('../../src/Controllers/UserController')

router.post('/register', register);
router.post('/login', login);
router.get('/user', authentication([{path: 'country', select: 'countryId name'}]), getUser);

module.exports = router;

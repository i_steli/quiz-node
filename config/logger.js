const { createLogger, format, transports, addColors } = require('winston');
const path = require('path');
const { combine, timestamp, label, printf } = format;
require('winston-daily-rotate-file');

console.log('logger', path.join(path.dirname(__dirname)));

const myFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = createLogger({
    format: combine(
        label({label: 'API'}),
        timestamp(),
        myFormat
    ),
    defaultMeta: {service: 'user-service'},
    transports: [
        new transports.Console(),
        new transports.DailyRotateFile({
            level: 'error',
            filename: path.join(path.dirname(__dirname), 'var', 'logs', 'error-%DATE%.log'),
            datePattern: 'YYYY-MM-DD',
            zippedArchive: true,
            maxSize: '20m',
            maxFiles: '14d',
        }),
        new transports.DailyRotateFile({
            level: 'info',
            filename: path.join(path.dirname(__dirname), 'var', 'logs', 'log-%DATE%.log'),
            datePattern: 'YYYY-MM-DD',
            zippedArchive: true,
            maxSize: '20m',
            maxFiles: '14d',
        })
    ]
});

module.exports = logger;
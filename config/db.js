const mongoose = require('mongoose');

exports.mongoDB = async function () {
    try {
        const conn = await mongoose.connect(process.env.MONGO_URI, {
            dbName: 'quiz',
            replicaSet: 'rs0',
        })
        console.log(`MongoDB connected to host ${conn.connection.host} and db ${conn.connection.name}`)
    } catch (e) {
        console.log(`Database error: ${e}`);
    }
}